import { render, screen } from '@testing-library/react';
import LocationDetils from './LocationDetails';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

test('renders component', () => {
    render(<LocationDetils  />);
    const element = screen.getByTitle('Loading...');
    expect(element).toBeInTheDocument();
}) 
