import { render, screen } from '@testing-library/react';
import Parameters from './Parameters';

test('renders component', () => {
    const params = [
        {
          "id": 35134,
          "unit": "ppm",
          "count": 10702,
          "average": 0.0273215052519155,
          "lastValue": 0.0046552,
          "parameter": "o3",
          "displayName": "O₃",
          "lastUpdated": "2021-09-21T14:00:00+00:00",
          "parameterId": 10,
          "firstUpdated": "2020-10-16T01:00:00+00:00"
        }]
    render(<Parameters  params={params} />);
    const element = screen.getByText('Parameters');
    expect(element).toBeInTheDocument();
}) 
