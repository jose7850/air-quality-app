import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

const Parameters = ({params}) => {
   return (
       <Paper className="param-container" elevation={3}>
            <h2>Parameters</h2>
            <TableContainer>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Parameter</TableCell>
                            <TableCell>Average</TableCell>
                            <TableCell>Count</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {params.map((param) => (
                        <TableRow
                        key={param.id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {param.displayName}
                            </TableCell>
                            <TableCell>{renderAverage(param)}</TableCell>
                            <TableCell>{param.count}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
       </Paper>
   ) 
}

const renderAverage = (param) => {
    return `${param.average.toFixed(4)} (${param.unit})`
} 

export default Parameters;