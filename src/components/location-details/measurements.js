import {useState, useEffect} from 'react';
import Paper from '@mui/material/Paper';
import { Bubble } from 'react-chartjs-2';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import CircularProgress from '@mui/material/CircularProgress';
import 'chartjs-adapter-date-fns';
import {
    Chart as ChartJS,
    LinearScale,
    TimeScale,
    PointElement,
    Tooltip,
    Legend,
  } from 'chart.js';

import OpenAQ from '../../apis/OpenAQ';

ChartJS.register(TimeScale, LinearScale, PointElement, Tooltip, Legend);

const Measurements = ({locationId, parameters}) => {
    const [selectedParam, setSelectedParam] = useState(parameters[0]);
    const [tableData, setTableData] = useState(data);
    const [response, loading] = useGetMeasurements(locationId, selectedParam);

    
    const handleChangeSelect = (event) => {
        setSelectedParam(event.target.value)
        setTableData(processData)
    }

    if(loading){
        return <div className="spinner"><CircularProgress title="Loading..." /></div>;
    }
    
    
    const processData = trasnformData(response.results)
    processData.datasets[0].label = selectedParam;
    

    return (
        <Paper className="param-container" elevation={3}>
            <h2>Measurements</h2>
            <div className="filter">
                <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="filter-entity">Filter</InputLabel>
                <Select 
                    labelId="filter-entity"  
                    id="filter-entity"
                    value={selectedParam}
                    label="Filter"
                    onChange={handleChangeSelect}
                >
                    { parameters.map((param) => <MenuItem key={param} value={param}><em>{param}</em></MenuItem>)}
                </Select>
                </FormControl>
            </div>
            { tableData && 
                <Bubble data={tableData} options={options} datasetIdKey="id" />
            }
        </Paper>
    )
}

const trasnformData = (measurements) => {
    const tableData = measurements.map(measurement => {
        return {y: measurement.value, x: measurement.date.local, r: 3 }
    })
    data.datasets[0].data = tableData;
    return data;
}

const options = {
    scales: {
        y: {
            beginAtZero: true,
        },
        x: {
            type: 'time',
            time: {
                unit: 'day',
                tooltipFormat: 'MMM/dd/yy hh:mm'
            }
        }
    },
  };
  
const data = {
datasets: [
    {
    label: 'so2',
    data: [
    {
        x: "2021-09-23T14:00:00-05:00",
        y: 0.003,
        r: 2
    },
    {
        x: "2021-09-22T13:00:00-05:00",
        y: 0.005,
        r: 2
    }
    ],
    backgroundColor: 'rgba(53, 162, 235, 1)',
    }
],
};

const useGetMeasurements = (locationId, parameter) =>{
    const [response, setResponse] = useState({});
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const getLocationDetails = async (locationId, parameter) => {
            setLoading(true)
            try{
                const params = {
                    location_id: locationId,
                    parameter
                }
                const response = await OpenAQ.get(`measurements/`, {params}); 
                setResponse(response.data);
            } catch(error) {
                console.log(error)
            } finally  {
                setLoading(false);
            }
            
        }
        getLocationDetails(locationId, parameter);
    }, [locationId, parameter])

    return [response, loading]
}

export default Measurements;