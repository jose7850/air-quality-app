import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import Chip from '@mui/material/Chip';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import './LocationDetails.css';
import OpenAQ from '../../apis/OpenAQ';
import Parameters from './Parameters';
import Measurements from './measurements';

const LocationDetails = () => {
    const { id } = useParams();
    const [response, loading] = useGetLocationsDetails(id);
    const navigate = useNavigate();

    if(loading) {
        return <div className="spinner"><CircularProgress title="Loading..." /></div>;
    }

    const location = response.results[0];
    
    return(
        <div className='container'>
            <div className="main">
                <div className='back'>
                    <IconButton aria-label="back" size="medium" onClick={() => navigate('/')}> 
                        <ArrowBackIcon fontSize="inherit" />
                    </IconButton>
                </div>
                <span>Location</span>
                <h3 className='title' title={location.name}>{location.name}</h3>
                <div > in {location.city}, {location.country}</div> 
                <div className="chips">
                    <Chip
                        title={'Entity: ' + location.entity}
                        color="primary" variant="outlined"
                        label={location.entity}
                    />
                    <Chip
                        title={'Sensor type: ' + location.sensorType}
                        color="primary" variant="outlined"
                        label={location.sensorType}
                    />
                </div>
            </div>
            <Grid sx={{ flexGrow: 1 }} container spacing={2} >
                <Grid item md={3} xs={12}>
                    <Card sx={{  height:  350}}>
                        <CardContent>
                            <h3>Details</h3>
                            <strong className='mesuarements'>{location.measurements}</strong>
                            <div>Measurements</div>
                            <div className='coords'>Coordinates</div>
                            <div className='txt-darkgray'>{'N' + location.coordinates.latitude + ', E' + location.coordinates.longitude}</div>
                            <div>Project Dates</div>
                            <div className='txt-darkgray'>{formatDate(location.firstUpdated) + ' - ' + formatDate(location.lastUpdated)}</div>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={6} xs={12}>
                    <Card sx={{  height:  350}}>
                        <CardContent>
                            <h3>Latest Measurements</h3>
                            <Grid sx={{ flexGrow: 1 }} container spacing={2} >
                                {renderMeasurements(location.parameters)}
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={3} xs={12}>
                    <Card sx={{  height:  350}}>
                        <CardContent>
                            <h3>Source</h3>
                            <div>{renderSources(location.sources)}</div>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Measurements locationId={id} parameters={getParametersArray(location.parameters)} />
            <Parameters params={location.parameters}></Parameters>
        </div>
    )
}

const renderMeasurements = (parameters) => {
    return parameters.map((param)=>{
        return (
            <Grid item md={4} key={param.id}>
                <div className='param-card'>
                    <div>{param.displayName}</div>
                    <h4 className='last-value'>{param.lastValue.toFixed(4)}</h4>
                    <strong>{param.unit}</strong>
                    <div>{formatDate(param.lastUpdated)}</div>
                </div>
            </Grid>
        )
    })
}

const renderSources = (sources) => {
    return sources.map( source => {
        return (source.url) ? <a key={source.name} href={source.url} target="_top" title="Go to source">{source.name}</a> : ''
    })
}

const formatDate = (date) => {
    return new Date(date).toLocaleDateString();
}

const useGetLocationsDetails = (locationId) =>{
    const [response, setResponse] = useState({});
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const getLocationDetails = async (locationId) => {
            try{
                const response = await OpenAQ.get(`locations/${locationId}`); 
                setResponse(response.data);
            } catch(error) {
                console.log(error)
            } finally  {
                setLoading(false);
            }
            
        }
        getLocationDetails(locationId);
    }, [locationId])

    return [response, loading]
}

const getParametersArray = (parameters) => {
    return parameters.map((parameter) => parameter.parameter);
}

export default LocationDetails;