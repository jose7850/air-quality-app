import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Chip from '@mui/material/Chip';
import Button from '@mui/material/Button';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { useNavigate } from 'react-router-dom';
import './LocationCard.css';

const LocationCard = ({location}) => {
    const navigate = useNavigate();
    return (
        <Card sx={{ minWidth: 275, height:  400}}>
            <CardContent className='location-card'> 
                <h4 className='title' title={location.name}>{location.name}</h4>  
                <div >Location ID {location.id}</div>  
                <div >in {location.city}, {location.country}</div> 

                <div className="chips">
                    <Chip
                        title={'Entity: ' + location.entity}
                        color="primary" variant="outlined"
                        label={location.entity}
                    />
                    <Chip
                        title={'Sensor type: ' + location.sensorType}
                        color="primary" variant="outlined"
                        label={location.sensorType}
                    />
                </div>

                <dl className='location-detail'>
                    <dt >First Update</dt>
                    <dd>{formatDate(location.firstUpdated)}</dd>
                    <dt >Last update</dt>
                    <dd>{formatDate(location.lastUpdated)}</dd>
                    <dt>Parameters</dt>
                    <dd className='parameters'>{getParametersString(location)}</dd>  
                    <dt>Mesurements</dt>
                    <dd>{location.measurements}</dd>  
                    <dt>Source</dt> 
                    <dd>{renderSources(location.sources)}</dd>
                </dl>       
                <Button 
                    className='more-details' 
                    variant="outlined" 
                    color="primary"
                    onClick={() => navigate(`locations/${location.id}`)}
                >
                    More details <ArrowForwardIosIcon sx={{ fontSize: 12 }} />
                </Button>
            </CardContent>
        </Card> 
        
    );
}

const renderSources = (sources) => {
    return sources.map( source => {
        return (source.url) ? <a key={source.name} href={source.url} target="_top" title="Go to source">{source.name}</a> : ''
    })
}

const formatDate = (date) => {
    return new Date(date).toLocaleDateString();
}

const getParametersString = (location) => {
    return location.parameters.reduce((prevParam, currentParam) => {
        if (!prevParam) {
            return currentParam.displayName;
        }
        return `${prevParam}, ${currentParam.displayName}`;
    }, '');
}

export default LocationCard;