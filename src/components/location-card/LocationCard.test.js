import { render, screen } from '@testing-library/react';
import LocationCard from './LocationCard';
import * as locationMock from '../../mockData/locationMock.json'

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

test('renders component', () => {
    render(<LocationCard  location={locationMock} />);
    const element = screen.getByText(locationMock.name);
    expect(element).toBeInTheDocument();
}) 
