import { render, screen } from '@testing-library/react';
import LocationsGrid from './LocationsGrid';

test('renders component', () => {
    render(<LocationsGrid  />);
    const element = screen.getByTitle('Loading...');
    expect(element).toBeInTheDocument();
}) 
