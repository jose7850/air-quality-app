import * as React from 'react';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from "@mui/material/InputLabel";
import Grid from '@mui/material/Grid';
import FormControl from "@mui/material/FormControl";
import CircularProgress from '@mui/material/CircularProgress';

import './LocationsGrid.css'
import LocationCard from '../location-card/LocationCard';
import OpenAQ from '../../apis/OpenAQ';

class LocationsGrid extends React.Component {

    state = {
        locations: null,
        metadata: null,
        loading: true, 
        select: ''
     }
   
    getLocations = async (entity) => {
        const params = { 
            limit: 30,
            page: 1,
            metadata: true,
            country: 'MX',
            order_by: 'city',
            sort: 'desc', 
        }
        if(entity){
            params.entity = entity
        }
        const response = await OpenAQ.get("locations", {params})
        const locations = response.data;
        return locations;
    }

    async componentDidMount() {
        const response = await this.getLocations();
        this.setState({locations: response.results, metadata: response.metadata, loading: false, select: ''});
    }

    handleChangeSelect = async (event) => {
        const response = await this.getLocations(event.target.value);
        this.setState({locations: response.results, metadata: response.metadata, select: event.target.value});
    }

    renderLocations = (locations) => {
        if(locations && locations.length > 0) {
            return locations.map((location) => (
                <Grid item md={4} xs={12} key={location.id}>
                    <LocationCard location={location}></LocationCard>
                </Grid>
            ))
        } else {
            return <div className='locations-error'>No locations were found!</div>
        }
    }

    render() {
        const { loading, locations, select } = this.state;

        if ( loading ) {
            return (
                <div className="spinner">
                    <CircularProgress title="Loading..." />
                </div>
            );
        }

        return (
            <div className="locations-grid">
                <div className="cotainer">
                    <div className="filter">
                        <FormControl sx={{ m: 1, minWidth: 120 }}>
                        <InputLabel id="filter-entity">Filter</InputLabel>
                        <Select 
                            labelId="filter-entity"  
                            id="filter-entity"
                            value={select}
                            label="Filter"
                            onChange={this.handleChangeSelect}
                        >
                            <MenuItem value=""><em>None</em></MenuItem>
                            <MenuItem  value="government">Government</MenuItem>
                            <MenuItem  value="community">Community</MenuItem>
                            <MenuItem  value="research">Research</MenuItem>
                        </Select>
                        </FormControl>
                    </div>
                    <Grid sx={{ flexGrow: 1 }} container spacing={2} >
                        {this.renderLocations(locations)}
                    </Grid>          
                </div>
            </div>
        );
    }
}

export default LocationsGrid;