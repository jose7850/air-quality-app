import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
  import AppBar from '@mui/material/AppBar';
  import Toolbar from '@mui/material/Toolbar';
  import Typography from '@mui/material/Typography';

import LocationsGrid from './components/locations-grid/LocationsGrid';
import './App.css';
import LocationDetails from './components/location-details/LocationDetails';

const App = () => {
    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h4" component="div" sx={{ flexGrow: 1 }}>
                        Air quality App
                    </Typography>
                </Toolbar>
            </AppBar>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<LocationsGrid />} />
                    <Route path="/locations/:id" element={<LocationDetails />} />
                </Routes>
            </BrowserRouter>
        </div> 
    );
}

export default App;